<!DOCTYPE html>
<html>
<head>
    <title>Tugas laravel View</title>
</head>
<body>
    <header>
        <h2>Hi, Selamat Datang di Blog Saya</h2>
        <nav>
            <a href="/blog">HOME</a>
            |
            <a href="/blog/tentang">TENTANG</a>
            |
            <a href="/blog/kontak">KONTAK</a>
        </nav>
    </header>
    <hr/>
    <br/>
    <br/>

    <!-- bagian judul halaman blog -->
    <h3> <?php echo $__env->yieldContent('judul_halaman'); ?> </h3>

    <!-- bagian konten blog -->
    <?php echo $__env->yieldContent('konten'); ?>

    <br/>
    <br/>
    <hr/>
</body>
</html>
<?php /**PATH C:\xampp\htdocs\sellypwl\resources\views/master.blade.php ENDPATH**/ ?>