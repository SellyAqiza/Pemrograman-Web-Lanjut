<!-- Menghubungkan dengan view template master-->
@extends('master')

<!-- Isi judul halaman-->
<!-- Isi section pendek-->
@section('judul_halaman', 'Halaman Kontak')


<!-- Isi bagian konten-->
<!-- Isi section panjang-->
@section('konten')

    <p>Hi, Selamat Datang di Halaman Kontak Saya</p>
    <table class="table1">
        <tr>
            <th>No</th>
            <th>Jenis Kontak</th>
            <th>Nama Kontak</th>
        </tr>
        <tr>
            <td>1</td>
            <td>Email</td>
            <td>sellyaqiza@gmail.com</td>
        </tr>
        <tr>
            <td>2</td>
            <td>Instagram</td>
            <td>@SellyAqiza</td>
        </tr>
        <tr>
            <td>3</td>
            <td>No. HP</td>
            <td>087-810-246-946</td>
        </tr>
    </table>
    </p> 
@endsection