<!DOCTYPE html>
<html>
<head>
    <title>Tugas laravel View</title>
</head>
<body>
    <header>
        <h2>Hi, Selamat Datang di Blog Saya</h2>
        <nav>
            <a href="/blog">HOME</a>
            |
            <a href="/blog/tentang">TENTANG</a>
            |
            <a href="/blog/kontak">KONTAK</a>
        </nav>
    </header>
    <hr/>
    <br/>
    <br/>

    <!-- bagian judul halaman blog -->
    <h3> @yield('judul_halaman') </h3>

    <!-- bagian konten blog -->
    @yield('konten')

    <br/>
    <br/>
    <hr/>
</body>
</html>
